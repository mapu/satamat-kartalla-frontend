import * as React from 'react'
import Lander from './components/Lander'
import { BrowserRouter } from 'react-router-dom'
import { Route } from 'react-router-dom'
import './App.css'

const App = () => {
  return (
    <BrowserRouter>
      <div className="App">
        <Route exact path="/" component={Lander} />
      </div>
    </BrowserRouter>
  )
}

export default App
