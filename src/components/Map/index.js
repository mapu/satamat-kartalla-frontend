import * as React from 'react'
import { Map, TileLayer } from 'react-leaflet'

const DEFAULT_VIEWPORT = {
  center: [60.192059, 24.945831],
  zoom: 10
}

class MapContainer extends React.Component {
  state = {
    viewport: DEFAULT_VIEWPORT
  }

  onViewportChanged = viewport => {
    this.setState({ viewport })
  }

  handleClick = e => {
    this.props.handleMapClick(e)
  }

  handleLocationFound = e => {
    global.console.log('location found', e)
  }

  render() {
    return (
      <Map
        onViewportChanged={this.onViewportChanged}
        viewport={this.state.viewport}
        onClick={this.handleClick}
        onLocationfound={this.handleLocationFound}
      >
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
      </Map>
    )
  }
}

export default MapContainer
