import * as React from 'react'
import MapContainer from '../Map'
import './index.css'
import SelectedPoint from '../SelectedPoint'

class Lander extends React.Component {
  state = {
    selectedPoint: null
  }

  handleMapClick = selectedPoint => {
    global.console.log('kekeke', selectedPoint)
    this.setState({
      selectedPoint
    })
  }

  render() {
    return (
      <div>
        <MapContainer handleMapClick={this.handleMapClick} />

        <div style={styles}>
          <SelectedPoint info={this.state.selectedPoint} />
        </div>
      </div>
    )
  }
}

export default Lander

const styles = {
  marginTop: '20px'
}
