import PropTypes from 'prop-types'
import React from 'react'

const propTypes = {
  info: PropTypes.object
}

const defaultTypes = {
  info: null
}

const SelectedPoint = ({ info }) => {
  const latlng = info && info.latlng

  return (
    <div className="map-information">
      {info && (
        <React.Fragment>
          <div>Valittuna:</div>
          <div>latitude: {latlng.lat}</div>
          <div>longitude: {latlng.lng}</div>
        </React.Fragment>
      )}
    </div>
  )
}

export default SelectedPoint

SelectedPoint.propTypes = propTypes
SelectedPoint.defaultProps = defaultTypes
